#include <stdio.h>
#include <stdlib.h>

//Ayala Chacón, Cristian Aarón Carnet:AC16053

void main() {
    int fil = 0, col = 0, aux = 0, size;
    int i = 0, j = 0, contador = 0;
    printf("Ingrese el tamaño de las matrices nxn: \n");
    scanf("%d", &size);
    if (size > 1) {



        int matriz1[size][size];
        int matriz2[size][size];
        int matriz_resultante[size][size];

        //LLENANDO MATRIZ 1
        printf("\nIngrese la matriz 1:\n");
        for (fil = 0; fil < size; fil++) {
            for (col = 0; col < size; col++) {
                printf("Posicion (%d,%d): ", fil + 1, col + 1);
                scanf("%d", &matriz1[fil][col]);

            }
        }

        //LLENANDO MATRIZ 2
        printf("\nIngrese la matriz 2:\n");
        for (fil = 0; fil < size; fil++) {
            for (col = 0; col < size; col++) {
                printf("Posicion (%d,%d): ", fil + 1, col + 1);
                scanf("%d", &matriz2[fil][col]);
            }
        }

        //MOSTRAR MATRIZ 1
        printf("\n Matriz 1:\n");
        for (fil = 0; fil < size; fil++) {
            printf("[ ");
            for (col = 0; col < size; col++) {
                printf("%d ", matriz1[fil][col]);
            }
            printf("]");
            printf("\n");
        }
        //MOSTRAR MATRIZ 2 
        printf("\n Matriz 2:\n");
        for (fil = 0; fil < size; fil++) {
            printf("[ ");
            for (col = 0; col < size; col++) {
                printf("%d ", matriz2[fil][col]);
            }
            printf("]");
            printf("\n");
        }

        //CREARA LA MATRIZ RESULTANTE
        printf("\nLa matriz resultante es: \n");
        for (i = 0; i < size; i++) {
            printf("[ ");
            for (j = 0; j < size; j++) {
                matriz_resultante[i][j] = 0;
                for (int h = 0; h < size; h++) {
                    matriz_resultante[i][j] = matriz_resultante[i][j]+(matriz1[i][h] * matriz2[h][j]);
                }
                printf("%d ", matriz_resultante[i][j]);
            }
            printf("]");
            printf("\n");
        }

        //Da cuales son primos y en que eposicion
        printf("\n");
        contador = 0;
        int cantidadPrimos=0;
        for (fil = 0; fil < size; fil++) {
            for (col = 0; col < size; col++) {
                for (j = 1; j <= matriz_resultante[fil][col]; j++) {
                    if (matriz_resultante[fil][col] % j == 0) {
                        contador++;
                    }
                }
                if (contador == 2) {
                        printf("Número primo:%d y se encuentra en la posición (%d,%d)\n", matriz_resultante[fil][col], fil + 1, col + 1);
                        cantidadPrimos++;
                    }
            }
        }

        if (cantidadPrimos = 0) {
            printf("No se encontraron números primos");
        } else {
            int vPrimos[contador];
            i = 0;
            for (fil = 0; fil < size; fil++) {
                for (col = 0; col < size; col++) {
                    for (j = 1; j <= matriz_resultante[fil][col]; j++) {
                        if (matriz_resultante[fil][col] % j == 0) {
                            contador++;
                        }
                    }
                     if (contador == 2) {
                            vPrimos[i] = matriz_resultante[fil][col];
                            i++;
                        }
                }
            }
            contador = i;
            for (i = 0; i < contador; i++) {
                printf("\nVector en la posición %d: %d\n", i + 1, vPrimos[i]);
            }
        }
    } else {
        printf("Tamaño de la matriz invalido");
    }
}

